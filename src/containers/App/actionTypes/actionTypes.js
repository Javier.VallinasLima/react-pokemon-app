const Types = {
  SET_POKEMON_LIST: 'SET_POKEMON_LIST',
  SET_POKEMON_DETAIL: 'SET_POKEMON_DETAIL',
};

export default Types;
