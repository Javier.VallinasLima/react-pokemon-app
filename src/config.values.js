export default {
  SELECTION: {
    FILTERING: {
      INPUT_SEARCH_DELAY: 500,
    },
    PAGINATION: {
      ITEMS_PER_PAGE: 25,
      INITIAL_OFFSET: 0,
    },
  },
};
